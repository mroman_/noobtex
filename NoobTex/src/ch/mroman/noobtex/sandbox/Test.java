package ch.mroman.noobtex.sandbox;

import ch.mroman.noobtex.parsing.Parser;
import ch.mroman.noobtex.parsing.TextObject;

public class Test {
	public static void main(String[] args) {
		System.setProperty("renderTarget", "html");
		Parser p = new Parser("$hallo welt$ du /sack/!\n\nKewl.");
		for(TextObject tobj : p.parse()) {
			System.out.print(tobj.render());
		}
	}
}
