package ch.mroman.noobtex.renderers;

import ch.mroman.noobtex.parsing.objects.PlainObject;
import ch.mroman.noobtex.renderers.html.PlainRendererHtml;
import ch.mroman.noobtex.renderers.plain.PlainRendererPlain;

public class PlainRenderer implements Renderer<PlainObject> {
	
	private Renderer<PlainObject> implInstance = null;
	private static PlainRenderer instance = null;
	
	private PlainRenderer(String target) {
		implInstance = (Renderer<PlainObject>) RendererLoader.loadForTarget(target, "PlainRenderer");
	}
	
	public static PlainRenderer getInstance() {
		if(instance == null) {
			instance = new PlainRenderer(System.getProperty("renderTarget"));
		}
		return instance;
	}
	
	public String render(PlainObject t) {
		return implInstance.render(t);
	}
}
