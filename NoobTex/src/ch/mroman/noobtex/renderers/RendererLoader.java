package ch.mroman.noobtex.renderers;

import ch.mroman.noobtex.parsing.objects.BoldObject;

public class RendererLoader {
	public static Object loadForTarget(String target, String rendererName) {
		assert(target.length() > 2);
		
		String suff = target.substring(0, 1).toUpperCase() + target.substring(1);
		try {
			return Class.forName("ch.mroman.noobtex.renderers."+target+"."+rendererName+suff).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
