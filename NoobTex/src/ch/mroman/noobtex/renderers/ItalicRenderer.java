package ch.mroman.noobtex.renderers;

import ch.mroman.noobtex.parsing.objects.ItalicObject;
import ch.mroman.noobtex.renderers.html.ItalicRendererHtml;
import ch.mroman.noobtex.renderers.plain.ItalicRendererPlain;

public class ItalicRenderer implements Renderer<ItalicObject> {
	
	private Renderer<ItalicObject> implInstance = null;
	private static ItalicRenderer instance = null;
	
	private ItalicRenderer(String target) {
		implInstance = (Renderer<ItalicObject>) RendererLoader.loadForTarget(target, "ItalicRenderer");
	}
	
	public static ItalicRenderer getInstance() {
		if(instance == null) {
			instance = new ItalicRenderer(System.getProperty("renderTarget"));
		}
		return instance;
	}
	
	public String render(ItalicObject t) {
		return implInstance.render(t);
	}
}
