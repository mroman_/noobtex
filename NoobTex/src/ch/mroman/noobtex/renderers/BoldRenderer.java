package ch.mroman.noobtex.renderers;

import ch.mroman.noobtex.parsing.objects.BoldObject;
import ch.mroman.noobtex.renderers.html.BoldRendererHtml;
import ch.mroman.noobtex.renderers.plain.BoldRendererPlain;

public class BoldRenderer implements Renderer<BoldObject> {
	
	private Renderer<BoldObject> implInstance = null;
	private static BoldRenderer instance = null;
	
	private BoldRenderer(String target) {
		implInstance = (Renderer<BoldObject>) RendererLoader.loadForTarget(target, "BoldRenderer");
	}
	
	public static BoldRenderer getInstance() {
		if(instance == null) {
			instance = new BoldRenderer(System.getProperty("renderTarget"));
		}
		return instance;
	}
	
	public String render(BoldObject t) {
		return implInstance.render(t);
	}
}
