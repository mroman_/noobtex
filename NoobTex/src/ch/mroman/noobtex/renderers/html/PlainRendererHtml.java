package ch.mroman.noobtex.renderers.html;

import ch.mroman.noobtex.parsing.objects.PlainObject;
import ch.mroman.noobtex.renderers.Renderer;

public class PlainRendererHtml implements Renderer<PlainObject> {
	
	public String render(PlainObject t) {
		return t.getText();
	}
	
}
