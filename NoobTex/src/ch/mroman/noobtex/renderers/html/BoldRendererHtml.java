package ch.mroman.noobtex.renderers.html;

import ch.mroman.noobtex.parsing.objects.BoldObject;
import ch.mroman.noobtex.renderers.Renderer;

public class BoldRendererHtml implements Renderer<BoldObject> {
	
	public String render(BoldObject t) {
		return "<b>"+ t.getText() +"</b>";
	}
	
}
