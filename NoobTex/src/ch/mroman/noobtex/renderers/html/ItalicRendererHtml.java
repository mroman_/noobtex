package ch.mroman.noobtex.renderers.html;

import ch.mroman.noobtex.parsing.objects.ItalicObject;
import ch.mroman.noobtex.renderers.Renderer;

public class ItalicRendererHtml implements Renderer<ItalicObject> {
	
	public String render(ItalicObject t) {
		return "<i>"+ t.getText() +"</i>";
	}
	
}
