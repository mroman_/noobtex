package ch.mroman.noobtex.renderers;

import ch.mroman.noobtex.parsing.TextObject;

public interface Renderer<T extends TextObject> {
	public String render(T t);
}
