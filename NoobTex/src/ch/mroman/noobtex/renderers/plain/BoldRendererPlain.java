package ch.mroman.noobtex.renderers.plain;

import ch.mroman.noobtex.parsing.objects.BoldObject;
import ch.mroman.noobtex.renderers.Renderer;

public class BoldRendererPlain implements Renderer<BoldObject> {
	
	public String render(BoldObject t) {
		return t.getText().toUpperCase();
	}
	
}
