package ch.mroman.noobtex.renderers.plain;

import ch.mroman.noobtex.parsing.objects.ItalicObject;
import ch.mroman.noobtex.renderers.Renderer;

public class ItalicRendererPlain implements Renderer<ItalicObject> {
	
	public String render(ItalicObject t) {
		return t.getText();
	}
	
}
