package ch.mroman.noobtex.renderers.plain;

import ch.mroman.noobtex.parsing.objects.PlainObject;
import ch.mroman.noobtex.renderers.Renderer;

public class PlainRendererPlain implements Renderer<PlainObject> {
	
	public String render(PlainObject t) {
		return t.getText();
	}
	
}
