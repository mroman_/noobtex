package ch.mroman.noobtex.parsing;

import java.util.*;

import ch.mroman.noobtex.parsing.objects.BoldObject;
import ch.mroman.noobtex.parsing.objects.ItalicObject;
import ch.mroman.noobtex.parsing.objects.PlainObject;

public class Parser {
	private Scanner scanner;
	private String currentPlainText = "";
	private List<TextObject> document = new ArrayList<>();
	private int index;
	
	public final static String pTextBold = "^\\$(.*)\\$";
	public final static String pTextItalic = "^/(.*)/";
	
	public Parser(String text) {
		scanner = new Scanner(text);
	}
	
	private void addCurrentPlainText() {
		document.add(new PlainObject(index, currentPlainText));
		currentPlainText = "";
	}
	
	private String removeFirstLast(String txt) {
		txt = txt.substring(1, txt.length()-1);
		return txt;
	}
	
	public List<TextObject> parse() {
		while(scanner.hasNextLine()) {
			index = scanner.advanceLine();
			
			if(scanner.isEmptyLine()) {
				currentPlainText += "\n";
			}
			
			while(!scanner.eol()) {
				if(scanner.hasNext(pTextBold)) {
					String txt = removeFirstLast(scanner.next(pTextBold));
					
					addCurrentPlainText();
					document.add(new BoldObject(index, txt));
				}
				else if(scanner.hasNext(pTextItalic)) {
					String txt = removeFirstLast(scanner.next(pTextItalic));
					
					addCurrentPlainText();
					document.add(new ItalicObject(index, txt));
				}
				else {
					currentPlainText += scanner.nextChar();
				}
			}
		}
		
		addCurrentPlainText();
		return document;
	}
}
