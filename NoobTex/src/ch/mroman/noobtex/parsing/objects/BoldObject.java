package ch.mroman.noobtex.parsing.objects;

import ch.mroman.noobtex.renderers.BoldRenderer;

public class BoldObject extends FormatObject {

	
	
	public BoldObject(int lineNo, String text) {
		super(lineNo, text);
	}

	public String render() {
		return BoldRenderer.getInstance().render(this);
	}

}
