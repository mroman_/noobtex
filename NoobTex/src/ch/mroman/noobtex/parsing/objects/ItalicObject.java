package ch.mroman.noobtex.parsing.objects;

import ch.mroman.noobtex.renderers.ItalicRenderer;

public class ItalicObject extends FormatObject {
	
	public ItalicObject(int lineNo, String text) {
		super(lineNo, text);
	}

	public String render() {
		return ItalicRenderer.getInstance().render(this);
	}

}
