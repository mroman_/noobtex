package ch.mroman.noobtex.parsing.objects;

import ch.mroman.noobtex.parsing.TextObject;

public abstract class FormatObject extends TextObject {
	
	private String text;
	
	public FormatObject(int lineNo, String text) {
		super(lineNo);
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
}
