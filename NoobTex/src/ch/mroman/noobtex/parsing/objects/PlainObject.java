package ch.mroman.noobtex.parsing.objects;

import ch.mroman.noobtex.renderers.PlainRenderer;

public class PlainObject extends FormatObject {

	
	
	public PlainObject(int lineNo, String text) {
		super(lineNo, text);
	}

	public String render() {
		return PlainRenderer.getInstance().render(this);
	}

}
