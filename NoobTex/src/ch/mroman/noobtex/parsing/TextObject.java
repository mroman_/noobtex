package ch.mroman.noobtex.parsing;

public abstract class TextObject {
	private int lineNo;
	
	public TextObject(int lineNo) {
		this.lineNo = lineNo;
	}
	
	public int getLine() {
		return lineNo;
	}
	
	public abstract String render();
}
