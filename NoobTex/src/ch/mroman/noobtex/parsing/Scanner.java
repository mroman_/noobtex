package ch.mroman.noobtex.parsing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Scanner {
	private String text;
	private String line;
	private String[] lines;
	private int index = 0;
	
	/**
	 * A scanner mostly based on regular expressions. 
	 * @param text The whole text.
	 */
	public Scanner(String text) {
		this.text = text;
		this.lines = this.text.split("\n");
	}
	
	/**
	 * Checks whether the current line starts with a given pattern
	 * @param pattern The pattern
	 * @return true if the pattern matches
	 */
	public boolean hasNext(String pattern) {
		Matcher m = Pattern.compile(pattern).matcher(line);
		if(m.find()) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * Drops leading spaces from the current line
	 */
	public void dropSpaces() {
		while(line.startsWith(" "))
			line = line.substring(1);
	}
	
	public boolean eol() {
		return line.length() == 0;
	}
	
	public boolean isEmptyLine() {
		return eol();
	}
	
	/**
	 * Returns the next 'token' based on a pattern
	 * @param pattern The pattern
	 * @return String or null if pattern did not match
	 */
	public String next(String pattern) {
		Matcher m = Pattern.compile(pattern).matcher(line);
		if(m.find()) {
			String group = m.group();
			line = line.substring(group.length());
			return group;
		}
		return null;
	}
	
	public String nextChar() {
		String chr = line.substring(0,1);
		line = line.substring(1);
		return chr;
	}
	
	/**
	 * Checks whether there's a next line available
	 * @return true if not end of text
	 */
	public boolean hasNextLine() {
		return index < lines.length;
	}
	
	/**
	 * Advances to the next line
	 * @return current line number
	 */
	public int advanceLine() {
		this.line = this.lines[index] + " ";
		dropSpaces();
		return index++;
	}
}
